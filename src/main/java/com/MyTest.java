package com;

public class MyTest {
    public static void main(String... args) {
        for (int i = 1; i <= 100; i++) {
            validate(i);
            System.out.println(i);
        }
    }

    private static void validate(int value) {
        if (value < 1 || value > 100) {
            throw new RuntimeException("Not valid element");
        }

    }
}
